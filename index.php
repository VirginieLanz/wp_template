<?php get_header(); ?>
    <div class="container">

        <!-- START: PAGE CONTENT -->
        <div class="blog">
            <div class="blog-grid">
                <div class="grid-sizer"></div>
                <div class="grid-item">
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                            <?php get_template_part( 'single' ); ?>
                    <?php endwhile; endif; ?>
                </div>
                </div><!-- .col-xs-6 -->
            </div><!-- .blog-grid -->
        <div class="pagination">
            <?php next_posts_link('Précédent') ?>
            <?php previous_posts_link('Suivant') ?>
        </div><!-- .pagination -->
        </div><!-- .blog -->
        <!-- END: PAGE CONTENT -->

    </div><!-- .container -->
    </div><!-- .content -->

    <footer class="footer">
<?php get_footer(); ?>