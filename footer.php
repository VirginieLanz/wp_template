<div class="footer-social">
    <ul class="social">
        <li><a class="ripple-centered" href="index.php" target="_blank"><i class="rsicon rsicon-facebook"></i></a></li>
        <li><a class="ripple-centered" href="index.php" target="_blank"><i class="rsicon rsicon-twitter"></i></a></li>
        <li><a class="ripple-centered" href="index.php" target="_blank"><i class="rsicon rsicon-linkedin"></i></a></li>
        <li><a class="ripple-centered" href="index.php" target="_blank"><i class="rsicon rsicon-google-plus"></i></a></li>
        <li><a class="ripple-centered" href="index.php" target="_blank"><i class="rsicon rsicon-dribbble"></i></a></li>
        <li><a class="ripple-centered" href="index.php" target="_blank"><i class="rsicon rsicon-instagram"></i></a></li>
    </ul>
</div>
</footer><!-- .footer -->
<?php wp_footer() ?>
</div><!-- .wrapper -->

<a class="btn-scroll-top" href="index.php#"><i class="rsicon rsicon-arrow-up"></i></a>
<div id="overlay"></div>
<div id="preloader">
    <div class="preload-icon"><span></span><span></span></div>
    <div class="preload-text">Loading ...</div>
</div>
<!-- Scripts -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
<script type="text/javascript" src="<?php echo get_bloginfo( 'template_directory' ); ?>/js/site.js"></script>
</body>
</html>
