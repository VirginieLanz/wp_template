<?php
// En l’absence d’autres fichiers présents dans le thème,
// WordPress se rabat toujours sur index.php.
// C’est pour ça que ce fichier est obligatoire, même si on ne l’utilise quasiment jamais. ?>

<?php get_header(); ?>
<div class="container">

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div class="row">
        <div class="col-xs-5">
            <?php get_template_part( 'content' ); ?>
        </div>
    </div>
    <?php endwhile; endif; ?>
    <!-- END: PAGE CONTENT -->

</div><!-- .container -->
</div><!-- .content -->

<footer class="footer">
    <?php get_footer(); ?>


