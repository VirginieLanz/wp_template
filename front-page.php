<?php get_header(); ?>
<div class="container">

    <!-- START: PAGE CONTENT -->
    <section id="about" class="section section-about">
        <div class="animate-up">
            <div class="section-box">
                <div class="profile">
                    <div class="row">
                        <div class="col-xs-5">
                            <div class="profile-photo"><img
                                        src="<?php the_field( 'photo_profile') ?>"
                                        alt="<?php the_field( 'prenom'); ?>&nbsp;<?php the_field( 'nom'); ?>"/>
                            </div>
                        </div>
                        <div class="col-xs-7">
                            <div class="profile-info">
                                <div class="profile-preword"><span>Hello</span></div>
                                <h1 class="profile-title"><span>I'm </span><?php echo get_bloginfo( 'name' ); ?></h1>
                                <h2 class="profile-position"><?php echo get_bloginfo( 'description' ); ?></h2></div>
                            <ul class="profile-list">
                                <li class="clearfix">
                                    <strong class="title">Age</strong>
                                    <span class="cont"><?php the_field( 'age'); ?></span>
                                </li>
                                <li class="clearfix">
                                    <strong class="title">Address</strong>
                                    <span class="cont"><?php the_field( 'adress'); ?></span>
                                </li>
                                <li class="clearfix">
                                    <strong class="title">E-mail</strong>
                                    <span class="cont"><a href="mailto:<?php echo get_bloginfo( 'admin_email' ); ?>"><?php echo get_bloginfo( 'admin_email' ); ?></a></span>
                                </li>
                                <li class="clearfix">
                                    <strong class="title">Phone</strong>
                                    <span class="cont"><?php the_field( 'phone'); ?></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="profile-social">
                    <ul class="social">
                        <li><a class="ripple-centered" href="https://www.facebook.com" target="_blank"><i class="rsicon rsicon-facebook"></i></a></li>
                        <li><a class="ripple-centered" href="https://twitter.com" target="_blank"><i class="rsicon rsicon-twitter"></i></a></li>
                        <li><a class="ripple-centered" href="https://www.linkedin.com" target="_blank"><i class="rsicon rsicon-linkedin"></i></a></li>
                        <li><a class="ripple-centered" href="https://plus.google.com" target="_blank"><i class="rsicon rsicon-google-plus"></i></a></li>
                        <li><a class="ripple-centered" href="https://dribbble.com" target="_blank"><i class="rsicon rsicon-dribbble"></i></a></li>
                        <li><a class="ripple-centered" href="https://www.instagram.com" target="_blank"><i class="rsicon rsicon-instagram"></i></a></li>
                    </ul>
                </div>
            </div>

            <div class="section-txt-btn">
                <p><a class="btn btn-lg btn-border ripple" target="_blank" href="http://dev.novembit.com/rs_card/wp-content/uploads/2015/11/test-1.pdf">Download Resume</a></p>
                <p><span class="cont"><?php the_field( 'text_profile'); ?></span></p>
            </div>
        </div>
    </section><!-- #about -->
    <div class="col-sm-9 col-xs-6">
        <ul>
            <li><a href="<?php echo home_url() ?>#about">About</a></li>
            <li><a href="<?php bloginfo('url'); ?>/blog/">Blog</a></li>
            <li><a href="<?php bloginfo('url'); ?>/contact/">Contact</a></li>
        </ul>
    </div>

    <!-- END: PAGE CONTENT -->

</div><!-- .container -->
</div><!-- .content -->

<footer class="footer">

<?php get_footer(); ?>

