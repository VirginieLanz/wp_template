<!DOCTYPE html>
<html lang="en" class="theme-color-07cb79 theme-skin-light" <?php language_attributes();?>>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo get_bloginfo( 'name' ); ?></title>

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/ico" href="/img/favicon.png"/>

    <!-- Google Fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Fredoka+One">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic">

    <!-- Icon Fonts -->
    <link rel="stylesheet" type="text/css" href="/fonts/map-icons/css/map-icons.min.css">
    <link rel="stylesheet" type="text/css" href="/fonts/icomoon/style.css">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory'); ?>/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory'); ?>/colors/theme-color.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Modernizer for detect what features the user’s browser has to offer -->
    <script type="text/javascript" src="/js/libs/modernizr.js"></script>
    <?php wp_head() ?>
</head>

<body class="home header-has-img loading"<?php body_class(); ?>>

<?php wp_body_open(); ?>

<?php get_sidebar(); ?>

<div class="wrapper">
    <header class="header">
        <div class="head-bg" style="background-image: url('img/uploads/rs-cover.jpg')"></div>
            <div class="row">
                <div class="col-sm-3 col-xs-6">
                    <a href="<?php echo home_url( '/' ); ?>">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/rs-logo.png" alt="Logo">
                    </a>
                </div>
                <div class="col-sm-9 col-xs-6">
                    <div class="nav-wrap">
                        <nav id="nav" class="nav">
                            <!-- Menus -->
                            <?php
                            wp_nav_menu( array(
                                'theme_location' => 'primary',
                                'container'      => false,
                                'menu'     => 'nav d-flex justify-content-between',
                            ) );?>
                        </nav>

                        <button class="btn-mobile btn-mobile-nav">Menu</button>
                        <button class="btn-sidebar btn-sidebar-open"><i class="rsicon rsicon-menu"></i></button>
                    </div><!-- .nav-wrap -->
                </div>
        </div>
    </header><!-- .header -->
<div class="content">
