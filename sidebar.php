<div class="sidebar sidebar-fixed">
    <button class="btn-sidebar btn-sidebar-close"> <i class="rsicon rsicon-close"></i></button>

    <div class="widget-area">
        <aside class="widget widget-profile">
            <div class="profile-photo">
                <img src="img/uploads/rs-photo-v2.jpg" alt="Robert Smith"/>
            </div>
            <div class="profile-info">
                <h2 class="profile-title"><?php echo get_bloginfo( 'name' ); ?></h2>
                <h3 class="profile-position"><?php echo get_bloginfo( 'description' ); ?></h3>
            </div>
        </aside><!-- .widget-profile -->

        <aside class="widget widget_contact">
            <h2 class="widget-title">Contact Me</h2>
            <form class="contactForm" action="https://rscard.px-lab.com/html/php/contact_form.php" method="post">
                <div class="input-field">
                    <input class="contact-name" type="text" name="name"/>
                    <span class="line"></span>
                    <label>Name</label>
                </div>

                <div class="input-field">
                    <input class="contact-email" type="email" name="email"/>
                    <span class="line"></span>
                    <label>Email</label>
                </div>

                <div class="input-field">
                    <input class="contact-subject" type="text" name="subject"/>
                    <span class="line"></span>
                    <label>Subject</label>
                </div>

                <div class="input-field">
                    <textarea class="contact-message" rows="4" name="message"></textarea>
                    <span class="line"></span>
                    <label>Message</label>
                </div>

                <span class="btn-outer btn-primary-outer ripple">
						<input class="contact-submit btn btn-lg btn-primary" type="submit" value="Send"/>
					</span>

                <div class="contact-response"></div>
            </form>
        </aside><!-- .widget_contact -->

        <aside class="widget widget-popuplar-posts">
            <h2 class="widget-title">Archives</h2>
            <ul>
                <li>
                    <div class="post-media"><a href="index.php"><img src="img/uploads/thumb-78x56-1.jpg" alt=""/></a></div>
                    <h3 class="post-title"><a href="index.php"><?php wp_get_archives( 'type=monthly' ); ?></a></h3>
                </li>
            </ul>
        </aside><!-- .widget-archives-posts -->

        <aside class="widget widget-recent-posts">
            <h2 class="widget-title">Recent posts</h2>
            <ul>
                <li>
                    <div class="post-tag">
                        <a href="index.php">#Photo</a>
                        <a href="index.php">#Architect</a>
                    </div>
                    <h3 class="post-title"><a href="index.php">Standard Post Format With Featured Image</a></h3>
                    <div class="post-info"><a href="index.php"><i class="rsicon rsicon-comments"></i>56 comments</a></div>
                </li>
                <li>
                    <div class="post-tag">
                        <a href="index.php">#Photo</a>
                        <a href="index.php">#Architect</a>
                    </div>
                    <h3 class="post-title"><a href="index.php">Standard Post Format With Featured Image</a></h3>
                    <div class="post-info"><a href="index.php"><i class="rsicon rsicon-comments"></i>56 comments</a></div>
                </li>
                <li>
                    <div class="post-tag">
                        <a href="index.php">#Photo</a>
                        <a href="index.php">#Architect</a>
                    </div>
                    <h3 class="post-title"><a href="index.php">Standard Post Format With Featured Image</a></h3>
                    <div class="post-info"><a href="index.php"><i class="rsicon rsicon-comments"></i>56 comments</a></div>
                </li>
            </ul>
        </aside><!-- .widget-recent-posts -->

        <aside class="widget widget_categories">
            <h2 class="widget-title">Categories</h2>
            <ul>
                <li><a href="index.php" title="Architecture Category Posts">Architecture</a> (9)</li>
                <li><a href="index.php" title="Business Category Posts">Business</a> (16)</li>
                <li><a href="index.php" title="Creative Category Posts">Creative</a> (18)</li>
                <li><a href="index.php" title="Design Category Posts">Design</a> (10)</li>
                <li><a href="index.php" title="Development Category Posts">Development</a> (14)</li>
                <li><a href="index.php" title="Education Category Posts">Education</a> (9)</li>
            </ul>
        </aside><!-- .widget_categories -->
    </div><!-- .widget-area -->
</div><!-- .sidebar -->

