<?php

add_action( 'after_setup_theme', 'custom_theme_setup' );
function custom_theme_setup() {
    add_theme_support( 'title-tag' );
    register_nav_menus( array(
        'primary' => 'Menu principal',
    ) );
// Ajouter la prise en charge des images mises en avant
    add_theme_support( 'post-thumbnails' );
}


//Activation du thème enfant
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

// Ajouter automatiquement le titre du site dans l'en-tête du site
add_theme_support( 'title-tag' );


function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}


add_action( 'wp_enqueue_scripts', 'custom_scripts' );
function custom_scripts() {
    // Google Fonts
    wp_enqueue_style( 'googlefont-fredokaone', 'https://fonts.googleapis.com/css?family=Fredoka+One', array(), '1.0.0' );
    wp_enqueue_style( 'googlefont-opensans', 'https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic', array(), '1.0.0' );

    // Icon Fonts
    wp_enqueue_style( 'map-icons', get_template_directory_uri() . '/fonts/map-icons/css/map-icons.min.css' );
    wp_enqueue_style( 'icomoon', get_template_directory_uri() . '/fonts/icomoon/style.css' );

    // Styles
    wp_enqueue_style( 'bxslider', get_template_directory_uri() . '/js/plugins/jquery.bxslider/jquery.bxslider.css' );
    wp_enqueue_style( 'customscrollbar', get_template_directory_uri() . '/js/plugins/jquery.customscroll/jquery.mCustomScrollbar.min.css' );
    wp_enqueue_style( 'mediaelementplayer', get_template_directory_uri() . '/js/plugins/jquery.mediaelement/mediaelementplayer.min.css' );
    wp_enqueue_style( 'fancybox', get_template_directory_uri() . '/js/plugins/jquery.fancybox/jquery.fancybox.css' );
    wp_enqueue_style( 'carousel', get_template_directory_uri() . '/js/plugins/jquery.owlcarousel/owl.carousel.css' );
    wp_enqueue_style( 'theme', get_template_directory_uri() . '/js/plugins/jquery.owlcarousel/owl.theme.css' );
    wp_enqueue_style( 'option-panel', get_template_directory_uri() . '/js/plugins/jquery.optionpanel/option-panel.css' );

    // Chargement de nos styles
    wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'color', get_template_directory_uri() . '/colors/theme-color.css' );


    // Scripts
    wp_enqueue_script( 'oss1', 'https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js', array( 'jquery' ), '1', true );
    wp_enqueue_script( 'oss2', 'https://oss.maxcdn.com/respond/1.4.2/respond.min.js', array( 'jquery' ), '1', true );

    // Modernizer for detect what features the user’s browser has to offer
    wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/libs/modernizr.js', array( 'jquery' ), '1', true );

    //Ajout des JS (appelés dans le footer)
    //wp_enqueue_script('scripts', get_template_directory_uri() . 'https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js', array( 'jquery' ),  NULL, true);
    //wp_enqueue_script('scripts', get_template_directory_uri() . 'https://maps.googleapis.com/maps/api/js', array( 'jquery' ),  NULL, true);
    //wp_enqueue_script('scripts', get_template_directory_uri() . '/js/site.js', array( 'jquery' ),  NULL, true);

}

if ( function_exists( 'acf_add_options_page' ) ) {
    acf_add_options_page();
}


//Désactiver les commentaires
add_action('admin_init', function () {
    // Redirige les utilisateurs qui essayent d'aller sur la page des commentaires
    global $pagenow;

    if ($pagenow === 'edit-comments.php') {
        wp_redirect(admin_url());
        exit;
    }

    // Supprimer la metabox commentaires du tableau de bord
    remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');

    // Désactiver la prise en charge des commentaires dans les types de publication
    foreach (get_post_types() as $post_type) {
        if (post_type_supports($post_type, 'comments')) {
            remove_post_type_support($post_type, 'comments');
            remove_post_type_support($post_type, 'trackbacks');
        }
    }
});

// Close comments on the front-end
add_filter('comments_open', '__return_false', 20, 2);
add_filter('pings_open', '__return_false', 20, 2);

// Hide existing comments
add_filter('comments_array', '__return_empty_array', 10, 2);

// Remove comments page in menu
add_action('admin_menu', function () {
    remove_menu_page('edit-comments.php');
});

// Remove comments links from admin bar
add_action('init', function () {
    if (is_admin_bar_showing()) {
        remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
    }
});
