<?php get_header(); ?>
<article class="post">
    <h2><?php the_title(); ?></h2>

    <p class="post__meta">
        Publié le <?php the_time( get_option( 'date_format' ) ); ?>
        par <?php the_author(); ?>
    </p>

    <?php the_excerpt(); ?> <!--N'affiche qu'un extrait d'article-->

    <p>
        <a href="<?php the_permalink(); ?>" class="post__link">Lire la suite</a> <!--Permet d’afficher le lien vers l’article-->
    </p>
</article>
<?php get_footer(); ?>

