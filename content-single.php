<?php get_header(); //Page singulière qui va afficher le contenu d'un article ?>
    <div class="container">

        <!-- START: PAGE CONTENT -->
        <div class="row animate-up">
            <div class="col-sm-8">
                <main class="post-single">
                    <article class="post-content section-box">
                        <div class="post-inner">
                            <header class="post-header">
                                <div class="post-data">
                                    <div class="post-tag">
                                        <a href="single.php"><?php the_tags(); ?></a>
                                    </div>

                                    <div class="post-title-wrap">
                                        <h1 class="post-title"><?php the_title(); ?></h1>
                                        <time class="post-datetime" datetime="<?php echo get_the_date(); ?>>">
                                            <span class="day"><?php echo get_the_date( 'd' ) ?></span>
                                            <span class="month"><?php echo get_the_date( 'M' ) ?></span>
                                        </time>
                                    </div>
                                </div>
                            </header>

                            <article class="post">
                                <?php the_post_thumbnail(); ?>

                                <h1><?php the_title(); ?></h1>

                                <div class="post__meta">
                                    <a href="single.php"><i class="rsicon rsicon-user"></i>Publié le <?php the_date(); ?> par <?php the_author(); ?></a>
                                </div>

                                <div class="post__content">
                                    <?php the_content(); ?>
                                </div>
                            </article>

                            <footer class="post-footer">
                                <div class="post-share">
                                    <script type="text/javascript" src="https://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-503b5cbf65c3f4d8" async="async"></script>
                                    <div class="addthis_sharing_toolbox"></div>
                                </div>
                            </footer>
                        </div><!-- .post-inner -->
                    </article><!-- .post-content -->
                </main>
                <!-- .post-single -->
            </div>
        </div><!-- .row -->
        <!-- END: PAGE CONTENT -->

    </div><!-- .container -->
    </div><!-- .content -->

    <footer class="footer">
<?php get_footer(); ?>